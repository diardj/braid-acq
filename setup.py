#!/usr/bin/env python

from setuptools import setup
#from distutils.core import setup

rsrc="../resources/"
setup(name='braidSimu',
      version='1.0',
      description='Braid SImulations',
      author='Alexandra Steinhilber',
      author_email='alexandra.steinhilber@univ-grenoble-alpes.fr',
      url='https://gricad-gitlab.univ-grenoble-alpes.fr/saghiraa/braidpy',
      scripts=['braid'],
      packages=['braidpy'],
      package_dir={'braidpy':'braidpy'},
      package_data= {"braidpy": [rsrc+"chardicts/*",rsrc+'confusionMatrix/*',rsrc+"lexicon/*.csv",
                                 rsrc+'prototype/*.pkl']}
     )
