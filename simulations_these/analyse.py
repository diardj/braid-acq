# /usr/bin/env python3
# -*-coding:utf-8-*-

from simulations import *
braidPath = "../../braidpy/"
from braidpy.GUI.plot import plot

def df_to_csv(df,name,PM=None):
    """
    Saves the simulation dataframe in a csv format.
    :param df: simulation dataframe
    :param name: string. Basename of the csv file to be saved.
    :param PM: boolean. Set to True when the simulation was for pseudowords.
    """
    try:
        df.to_csv("csv/"+name+("PM" if PM==True else "M" if PM==False else "")+".csv")
    except:
        pass

def get_plot(name,PM=True):
    """
    :param name: string. Basename of the simulation
    :param PM: boolean. Set to True when the simulation was for pseudowords.
    :return: plot object from the plot class
    """
    return plot(name+"_PM_O"+('P' if PM else '')+".pkl")

def get_plot_success(name,PM=True,lenMin=0):
    """
    Prints success information about the simulation.
    :param name: string. Basename of the simulation.
    :param PM:  boolean. Set to True when the simulation was for pseudowords.
    :param lenMin: int. Minimum word length to be considered.
    """
    df=plot(name + "_PM_O" + ('P' if PM else '') + ".pkl").df_brut
    df = df[df.num == 0].copy()
    df['len'] = df.word.str.len()
    df = df[df.len>lenMin].copy()
    df['success'] = df['success'].astype(float)
    print(df.groupby('len').mean()['success'])
    print(df.mean()['success'])
    
def get_oral_plot(name):
    """
    Gets both plots (with and without prior phonological knowledge) and returns the corresponding plot object
    :param name: string. Basename of the simulation.
    :return: a plot object.
    """
    try:
        pt_M = get_plot(name,False)
        pt_PM = get_plot(name)
        pt_M.df_brut['oral']=True
        pt_PM.df_brut['oral']=False
        pt_M.df_brut=pt_M.df_brut.append(pt_PM.df_brut)
        return pt_M
    except:
        pass

def get_oral_df(name):
    """
    Gets both plots (with and without prior phonological knowledge) and returns the corresponding dataframe.
    :param name: string. Basename of the simulation.
    :return: the dataframe of the simulation
    """
    return get_oral_plot(name).df_brut

def compare_simu(name,values,variable_name=None):
    """
    Gets different simulations corresponding of different values (english,french) of some variable (language) and returns the simulation dataframe.
    :param name: string. Basename of the simulation.
    :param values: values for the variable name, one value for one simulation.
    :param variable_name: the variable which varies across simulations.
    :return: the simulation dataframe
    """
    try:
        variable_name = name if variable_name is None else variable_name
        df = {key : get_oral_df(name+'_'+str(key)) for key in values}
        for val in values:
            df[val][variable_name]=val
        return pd.concat(df.values(), ignore_index=True)
    except:
        pass

