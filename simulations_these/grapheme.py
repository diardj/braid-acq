# /usr/bin/env python3
# -*-coding:utf-8-*-

from analyse import *
from simulations import *


###### Définition du modèle #######

param, sim = model_definition()

###### Simulations #######

n_words=30
words={}
words["cons"],words["incons"]=get_consistency_lists(sim,n_words,4,8)
words["random"]= get_random_list(sim,n_words,4,8)
random_cons=get_list_consistency(words["random"])


def coherence_regularity_consistance():
    """
    Checks that all consistant words are regular and that all inconsistant words are irregular
    """
    Lexique_infra = pd.read_csv(braidPath + "resources/lexicon/Lexique_infra.csv").groupby('word').first()
    all([Lexique_infra.loc[i,'countregTy_GP']==0 for i in words['cons']])
    all([Lexique_infra.loc[i,'countregTy_GP']==0 for i in words['incons']])




def run():
    """
    Runs the simulation for all types of words (regular, irregular, random)
    """
    param['expe'].update({"basename":"grapheme","n_expo":1,"test":{"context_sem":[False,True]}})
    for type,wds in words.items():
        param['expe']['basename']='grapheme_'+type
        simulate_success_rate(param,sim,liste=wds)
param['phono']["remove_stim"]=True
run()
param['phono']["remove_stim"]=False
run()


### Data generation #####
name="grapheme_retro"
df=compare_simu(name,['cons','random','incons'],"type")
df_to_csv(df,name)
















