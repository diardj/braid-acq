# /usr/bin/env python3
# -*-coding:utf-8-*-
import glob

import matplotlib.pyplot as plt
import numpy as np

from analyse import *
from simulations import *
from matplotlib.ticker import MaxNLocator


###### Définition du modèle #######

param, sim = model_definition()
l = [0, 1, 2, 5, 10, 50, 100, 200, 500,1000][::-1]
f=np.sort(list(set([int(i.split('_')[1]) for i in glob.glob('pkl/*freq*')])))


def taille_lex():
    """
    Calculates the size of the lexicon according to the minimum frequency to include the word in the lexicon, and then saves the results to a CSV file.
    :return: a dictionary `size_lex` which contains the frequency minimum as keys and the corresponding size of the lexicon as values.
    """
    lx=sim.model.ortho.lexical.df
    size_lex={}
    for i in f:
        size_lex[i]=len(lx[(lx.len==5)&(lx.freq>i)&(~lx.index.str.contains('_'))])
    print(lx[(lx.len==5)&(lx.freq>1000)&(~lx.index.str.contains('_'))])
    pd.DataFrame.from_dict(size_lex,orient="index").reset_index().rename({0:"taille","index":"freq_min"},axis=1).to_csv("csv/taille_lexique_freq_min_ville.csv")
    return size_lex
taille_lex()



###### Simulations #######

def simulate(freq_min,param,sim,n_words=50,liste=None):
    """
    Runs a simulation for a given minimal frequency and creates a simulation object.
    :param freq_min: minimal orthographic frequency to include a word in the lexicon
    :param param: parameters for the simulation
    :param sim: simulation objects
    :param n_words: number of words per length
    :param liste: liste of words to be read
    :return:
    """
    param['phono']["remove_stim"]=True
    param['model']['fMinOrtho']=freq_min
    param['expe'].update({"basename":"freq_large_"+str(freq_min),"n_expo":5,"test":{"context_sem":[False,True]}})
    simulate_success_rate(param,sim,n=n_words)
    param['phono']["remove_stim"]=False
    simulate_success_rate(param,sim,n=n_words)

n_words=100
lx=sim.model.ortho.lexical.df
for i in l:
    simulate(i,param,sim,n_words=n_words)


## Data generation ###########

df_to_csv(compare_simu("freq",f[::-1]),"taille_lexique",None)







