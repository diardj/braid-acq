---
title: Learning simulations
author: Alexandra Steinhilber
date: 18-01-2023
output: 
    html_document
---

# library
```{r}
library(dplyr)
library(ggplot2)
library(tidyr)
library(gridExtra)
path<-"/home/alexandra/braidpy/SimuThese/"
source(paste0(path,"variables.R"))
source(paste0(path,"generic_plots.R"))
```

# Données

```{r}
data_app = read.csv("/home/alexandra/braidpy/SimuThese/csv/app.csv") %>% select(-c(X)) %>%
                     mutate(success = if_else(success=="True", 1, 0))
group_by_app<-list("oral","context_sem")

```
```{r}
################################
# Figure 7.3 of my manuscript ##
################################

data_corr_app <- corr(data_app,group_by_app)
data_prop_corr <-prop(data_corr_app,group_by_app)


# Proportion of corrected items
p1 <- data_prop_corr %>% group_by(oral, succ_avant_corr) %>%
  ggplot(aes(x=succ_avant_corr, y=mean, fill=oral)) + bar +
  mid+ylab_proportion +legend_succ_x+ guides(fill="none")+legend_oral_fill+labs(title="(a)")
data_succ_corr<-succes(data_corr_app,group_by_app)

# Proportion of successful correction
p3 <- data_succ_corr %>% ggplot(aes(x=succ_avant_corr, y=mean, fill=oral)) +  bar + mid +
  ylab_succes + legend_succ_x + legend_oral_fill +guides(fill="none")+labs(title="(b)")
# taux d'acceptation
data_accept_corr <- accept(data_corr_app,group_by_app)

# Proportion of accepted corrections
p4 <- data_accept_corr %>% ggplot(aes(x=succ_corr, y=mean, fill=oral)) +  bar + mid +
  ylab_acceptation + legend_oral_fill + scale_x_discrete(name="prononciation après correction",labels=c("correcte","incorrecte"))+ ylim(0,1)+ guides(fill="none")+labs(title="(c)")# +scale_alpha_discrete(range = c(0.6, 1.0))

# Progression thanks to post-processing
data_progr<-progression(data_corr_app,group_by_app)
plot_progr <- data_progr %>%
  ggplot(aes(x=oral, y=mean,fill=oral)) +  bar +
  mid +legend_oral_fill+ylab_progression+ ylim(-0.05,0.1)+xlab("")+scale_x_discrete(labels=NULL)+labs(title="(d)")  #+theme(legend.position="none")

g<- grid.arrange(p1, p3, p4,plot_progr,
                 widths = c(10,10, 10,10),
                 layout_matrix = rbind(c(1, 2,3,4)))
ggsave(paste0(path,"img/app_corr_grid.png"),g)
```
```{r}
# Rate of correct assessment of phonological novelty at first exposure
# Figure 7.4 of my manuscript

data_app_dec <- data_app %>% group_by(word, oral,context_sem) %>%
  mutate(dec_correct=if_else(any((t==0)&(num==7)&(success==1)),"correcte","incorrecte")) %>%
  filter(num==4,t==0) %>%
  mutate(value=if_else((value=="True")&(oral=="False")|(value=="False")&(oral=="True"),1,0))

plot_PM_corr<-data_app_dec %>%
  group_by(context_sem,oral,dec_correct,t) %>%
  summarise(mean=mean(value)) %>%
  ggplot(aes(x=dec_correct, y=mean,fill=oral)) + bar + big+ facet_wrap(.~context_sem,labeller=context_labeller)+
  ylab_decision+legend_oral_fill+legend_succ_x+ylab("Taux de nouveauté phonologique positive")+ylim(0,1)+labs(title="(b)")

plot_PM<-data_app_dec %>%
  group_by(context_sem,oral,dec_correct) %>%
summarise(mean=mean(value)) %>%
  ggplot(aes(x=oral, y=mean,fill=oral)) + bar + big+facet_wrap(.~context_sem,labeller=context_labeller)+
  ylab_decision+legend_oral_x+ylab("Taux de nouveauté phonologique correcte")+ylim(0,1)+guides(fill="none")+labs(title="(a)")

g<- grid.arrange(plot_PM,plot_PM_corr,
                 widths = c(5,10),
                 layout_matrix = rbind(c(1, 2)))
ggsave(paste0(path,"img/app_PM.png"),g)


```
```{r}

# Reading times across exposures according to the decoding/learning success
# FIgure 7.5 of my manuscript

plot_temps_app <- data_app %>% decision_correcte_first_expo(group_by_app) %>%
  filter(num==5) %>%
  mutate(value = as.integer(as.character(value))/nchar(as.character(word))) %>%
  group_by(context_sem,oral,decision_corr,t) %>%
  summarise(mean=mean(value)) %>%
  mutate(t=t+1) %>% ggplot(aes(x=t, y=mean, color=oral,linetype=context_sem)) +
  line + big +xlab_expositions + ylab_temps +  ylim(0,150)+legend_oral_color + facet_grid(.~decision_corr) +
  scale_linetype_discrete(name="contexte", breaks=c("False","True"), labels=c("hors contexte","en contexte"))+labs(title="(b)") +guides(linetype="none",color="none")



data_app_dec <- data_app %>% group_by(word, oral,context_sem) %>%
  mutate(dec_correct=if_else(any((t==0)&(num==0)&(success==1)),"correct","incorrect"))
plot_temps_dec <- data_app_dec %>% temps(append(group_by_app,'dec_correct'),'dec_correct') +labs(title="(a)")

g<- grid.arrange(plot_temps_dec,plot_temps_app,ncol=2,widths=c(15,10))
ggsave(paste0(path,"img/app_temps_correct.png"),g)


```
```{r}
# Decoding without context
# Figure 7.6 of my manuscript


data_t0<- data_app %>% decision(group_by_app,t=0) %>% mutate(t=1)
data_t4<- data_app %>% decision(group_by_app,t=4) %>% mutate(t=5)
data_t1<- data_app %>% decision(group_by_app,t=1) %>% mutate(t=2)
data_t2<- data_app %>% decision(group_by_app,t=2) %>% mutate(t=3)
data_t3<- data_app %>% decision(group_by_app,t=3) %>% mutate(t=4)
plot_decodage <- decodage(data_app,group_by_app,FALSE) %>%
  filter(context_sem=="False") %>%
  ggplot(aes(x=oral, y=mean,fill=oral)) + geom_bar(stat="identity",position="stack",color="black")+
  big +ylab_decodage +legend_oral_x+ylim(0,1)+ legend_oral_fill+guides(fill="none")+labs(title="(a)")
plot_TAC <- rbind(data_t0,data_t1,data_t2,data_t3,data_t4) %>%
  filter(context_sem=="False") %>%
  ggplot(aes(x=factor(t), y=mean, fill=oral)) + bar + big+ylim(0,1)+
  ylab_decision+xlab_expositions +guides(fill="none")+facet_grid(.~oral,labeller=oral_labeller)+labs(title="(b)")
g <- grid.arrange(plot_decodage,plot_TAC,ncol=2,widths=c(5,15))
ggsave(paste0(path,"img/app_decodage_HC.png"),g)
```
```{r}
# Reading times across exposures, without context
# Figure 7.7 of my manuscript

data_app %>% filter(context_sem=="False")%>%
  filter(num == 5) %>%
  mutate(value = as.integer(as.character(value))/nchar(as.character(word))) %>%
  mutate(t=t+1) %>%
  group_by(.dots=append(group_by_app,'t')) %>%
  summarise(mean = mean(value)) %>%
  ggplot(aes(x=t, y=mean, color=oral))+ line + big +xlab_expositions + ylab_temps +ylim(0,200)+legend_oral_color
save_image("app_temps_HC.png")

```

