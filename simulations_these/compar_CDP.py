# /usr/bin/env python3
# -*-coding:utf-8-*-

from analyse import *
from simulations import *
import braidpy.lexicon as lx


###### Définition du modèle #######

param, sim = model_definition()

###### Simulations #######

lex_us=lx.simplify_phonemes(lx.simplify_letters(pd.read_csv(braidPath+'resources/lexicon/lexique_fr.csv')[['word','freq']])).groupby('word').first().reset_index()
lex_cdp=lx.simplify_phonemes(lx.simplify_letters(pd.read_csv(braidPath+'resources/lexicon/FrenchDB.txt',sep='\t',encoding='latin'))).groupby('word').first().reset_index()
liste=list(lex_us[(lex_us.word.isin(lex_cdp.word))&(lex_cdp.word.str.len()>2)].word.values)
liste=[i for i in liste if len(i)>2]
print(len(list(set(liste))))

param['phono']["remove_stim"]=True
param['phono']["top_down_influence"]=False
param['expe'].update({"basename":"compar_CDP_no_TD","n_expo":1,"test":{"context_sem":[False]},"res_fct_name":["phi"]})
simulate_success_rate(param,sim,liste=liste)
param['phono']["top_down_influence"]=True
param['expe']["basename"]="compar_CDP"
simulate_success_rate(param,sim,liste=liste)


def success_BA():
    """
    Prints results for the model BRAID-Acq
    """
    res = get_plot("compar_CDP").df_brut.groupby('word').first().reset_index()
    res=res[res.word.isin(liste)].copy()
    res.reset_index(drop=True).groupby('word').first().to_csv(braidPath+'SimuThese/csv/res_BA.csv')
    get_plot_success("compar_CDP",lenMin=2)
success_BA()

def success_CDP():
    """
    Prints results for the model CDP
    """
    res_cdp=lx.simplify_letters(pd.read_csv(braidPath+'SimuThese/CDPFrench_results.txt',sep='\t',encoding='utf-8',usecols=[0,10,11]).dropna())
    res_cdp=res_cdp[res_cdp.word.isin(liste)].groupby('word').first().reset_index().copy()
    res_cdp['len']=res_cdp.word.str.len()
    print(res_cdp.groupby('len').mean())
    res_cdp.reset_index(drop=True).to_csv(braidPath+'SimuThese/csv/res_cdp.csv')
success_CDP()














