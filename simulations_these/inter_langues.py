# /usr/bin/env python3
# -*-coding:utf-8-*-

from analyse import *
from simulations import *

###### Définition du modèle #######

param, sim = model_definition()


###### Simulations #######
def run(n_words):
    """
    Runs the simulation in all 3 languages (French, German, English)
    :param n_words: number of words per length
    """
    param['expe'].update({"n_expo":5,"test":{"context_sem":[False,True]}})
    param['model']['maxItem']=15000
    for langue in ["fr","ge","en"]:
        param['expe']["basename"]="langue_shift_"+langue
        param["model"]["langue"]=langue

        param['phono']["remove_stim"]=True
        simulate_success_rate(param,sim,n=n_words)

        param["phono"]["remove_stim"]=False
        simulate_success_rate(param,sim,n=n_words)

run(n_words=100)

#### Data generation ###########

name="langue"
df=compare_simu(name,['en','ge','fr'],name)
df_to_csv(df,name)

