# /usr/bin/env python3
# -*-coding:utf-8-*-
from collections import defaultdict

braidPath = "../../braidpy/"

from braidpy.expe import expe
from braidpy.simu import simu
import braidpy.utilities as utl
import pandas as pd
import pprint
pp = pprint.PrettyPrinter(width=100,compact=True)
utl.pandas_options()


def model_definition():
    """
    Defines the model used in the simulations and the parameters for the experiment.
    :return: all objects needed for the simulation.
    """
    simu_param={"level":"expe", "max_iter":2000}
    model_param={"langue":"fr", "path":braidPath, "sdA":1, 'Qa':1,'context_sem':True, "remove_lemma":True}
    ortho_param = {"learning":True, "remove_stim":True}
    phono_param = {"learning":True, "enabled":True, "remove_stim":False}
    expe_param ={"path":"./","res_fct_name":["phi","wfusion",'psi_score','success_correction','PM','t_tot','correction','pron_sans_corr','ld_phono','id_sans_corr'],"n_expo":1,"reinit":True,"print_res":True}
    simu_args = {}
    sim = simu(model_param, ortho_param, phono_param, simu_args, **simu_param)
    return {"expe":expe_param,"simu":simu_param,"model":model_param,"ortho":ortho_param,"phono":phono_param, "simu_args":simu_args},sim

def get_random_list(sim,nb,lenMin,lenMax):
    """
    Returns a list of randomly selected words for the simulations
    :param sim: the simulation object
    :param nb: number of words per length
    :param lenMin: minimum length for the simulus
    :param lenMax: minimum length for the simulus
    :return: the list of selected words
    """
    lx = sim.model.ortho.lexical.df[~sim.model.ortho.lexical.df.index.str.contains('~')].reset_index().groupby('word').first()
    hp=lx.groupby('pron').count()['idx'].reset_index()
    hp=list(hp[hp.idx>1].pron.values)
    random_words_list=list(lx[(lx.freq>0)&(~lx.pron.isin(hp))&(lx.len>=lenMin)&(lx.len<=lenMax)&(~lx.index.str.contains('_'))].groupby('len').sample(n=nb,random_state=23).index)
    return random_words_list

def get_regularity_lists(sim,nb,lenMin,lenMax):
    """
    Returns a list of regular and irregular selected words for the simulations
    :param sim: the simulation object
    :param nb: number of words per length
    :param lenMin: minimum length for the simulus
    :param lenMax: minimum length for the simulus
    :return: the list of selected words
    """
    Lexique_infra_original = pd.read_csv(braidPath + "resources/lexicon/Lexique_infra.csv")
    df=Lexique_infra_original
    df['len']=df.word.str.len()
    df=df[df.word.isin(sim.model.ortho.lexical.df.index)].copy()
    df=df[(df.len>=lenMin)&(df.len<=lenMax)].copy()
    df['reg'] = df['reg'].astype(float)
    reg=list(df[df.reg==1].groupby('len').sample(n=nb,random_state=22).word)
    irreg=list(df[df.reg==0].groupby('len').sample(n=nb,random_state=22).word)
    return reg,irreg

def get_cons_df(lenMin,lenMax):
    """
    extract the consistency for each word in the sublexicon selected.
    :param lenMin: minimum length for the simulus
    :param lenMax: minimum length for the simulus
    :return:
    """
    Lexique_infra = pd.read_csv(braidPath + "resources/lexicon/Lexique_infra_cons.csv")
    df=Lexique_infra
    df['len']=df.word.str.len()
    df=df[(df.len>=lenMin)&(df.len<=lenMax)&(df.freq>0)].astype({'cons_min':float}).groupby('word').first().reset_index().copy()
    df['comp'] = df.word.str.len() - df.pron.str.len()
    return df

def get_list_consistency(liste):
    """
    Retrieves the consistency of words in the list.
    :param liste: liste of words to retrieve the consistency
    :return: a list of consistency values
    """
    df=get_cons_df(4,8)
    return [df.set_index('word').loc[i].cons_min for i in liste if i in df.word.values]

def get_consistency_lists(sim,nb,lenMin,lenMax):
    """
    Extracts two lists of consistant and inconsistant words from the lexicon.
    :param sim: the simulation object
    :param nb: number of words per length
    :param lenMin: minimum length for the simulus
    :param lenMax: minimum length for the simulus
    :return: a list of consistent and inconsistent words
    """
    df=get_cons_df(lenMin,lenMax)
    df=df[(df.word.isin(sim.model.ortho.lexical.df.index))].copy()
    cons_df=df[(df['cons_min']>0.95)]
    incons_df=df[(df['cons_min']<0.05)]
    cons=list(cons_df.groupby('len').sample(n=nb,random_state=23).word)
    incons=list(incons_df.groupby('len').sample(n=nb, random_state=23).word)
    return cons,incons

def simulate_success_rate(param,sim,n=100,lenMin=4,lenMax=8,liste=None):
    """
    Runs a simulation : creates the simulation objects, extracts the list of words to be read, and simulates reading for each word accoring to the expe parameters.
    :param param: dictionary. The parameters of the simulation, used to create the object simu, model, ortho, phono and expe
    :param sim: the simulation object
    :param n: number of words per length
    :param lenMin: minimum length for the simulus
    :param lenMax: minimum length for the simulus
    :param liste: liste of words to be read
    :return:
    """
    par_sim=param['simu']
    sim = simu(param["model"], param["ortho"], param["phono"], **par_sim)
    liste = liste if liste is not None else get_random_list(sim,n,lenMin,lenMax)
    param['expe']['liste'] = liste
    exp = expe(simu=sim,store_simu=False,**param['expe'])
    exp.compare_param()

