# /usr/bin/env python3
# -*-coding:utf-8-*-

from analyse import *
from simulations import *


###### Définition du modèle #######

param, sim = model_definition()

###### Simulations #######

n_words=100


param['phono']["remove_stim"]=True
param['expe'].update({"basename":"app_retroaction_sem","n_expo":1,"test":{"context_sem":[False,True]}})
simulate_success_rate(param,sim,n=n_words)

param['phono']["remove_stim"]=False
simulate_success_rate(param,sim,n=n_words)


### Génération des données
df_to_csv(get_oral_df("app"),"app")
















