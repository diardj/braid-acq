# /usr/bin/env python3
# -*-coding:utf-8-*-

from analyse import *
from simulations import *

###### Définition du modèle #######

param, sim = model_definition()

###### Simulations #######
n_words=100

param["model"]["context_sem"]=True
def run():
    param['expe'].update({"basename":"context","n_expo":1,"test":{"p_sem":[1,5,10,100,1000],"N_sem":[1,5,10,100,1000]}})
    param['expe']['basename']='context_1'
    simulate_success_rate(param,sim,n=n_words)
param['phono']["remove_stim"]=True
run()
param['phono']["remove_stim"]=False
run()


#### Data generation ###########

df_to_csv(get_oral_df("context"),"context")







