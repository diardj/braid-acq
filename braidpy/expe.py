# -*- coding: utf-8 -*-
# General purpose libraries
import copy
# Scientific/Numerical computing

import itertools
# BRAID utlities
from braidpy.simu import simu
import logging
import os
import pdb
import pickle as pkl
import random
import gc
from time import time

import numpy as np
import pandas as pd

# on désactive le mode debug de numba et numpy
numba_logger = logging.getLogger('numba')
numba_logger.setLevel(logging.WARNING)
numpy_logger = logging.getLogger('numpy')
numpy_logger.setLevel(logging.WARNING)

class expe:
    """
    Experiment involving simulations from several stimuli, exposures, parameters
    """

    def __init__(self,simu=None, res_fct_name=None, path="./", basename="simu", test={},
            n_expo=1, reinit=True, lenMin=4,lenMax=8,n_stim_len=10,liste=None,
            word_action=None, word_action_value=None, simulation_action=None, last_expo_isolation=False,
            simulation_action_value=None, shuffle=False, print_res=False,store_simu=False):
        """
        :param simu: An instance of the simulation class
        :param res_fct_name: A string or a list of strings representing the name(s) of the function(s) that will be used to compute the results of the simulation.
            All the possibilities are listed in the function simu.one_res of class simu
        :param path: The root path.
        :param basename: The basename for the simulation files that will be saved, it will be completed according to simulation parameters (orthographically/phonologically novel ?)
        :param test: A dictionary containing the parameters that will be tested (keys), and the values that will be tested for each parameters (values). The cross-product of each combination of parameters will be tested.
            ex test={"Q": [1,2], "max_iter" :[500,1000]}
        :param n_expo: The number of exposures for each word, defaults to 1 (optional)
                           /!\ in the stored dataframe, t can be either a number of iterations or a number of exposures
                           but in the simulation it's possible to vary both the number of exposures and store one result per iteration simulated
                           the correspoding columns in the dataframe (t,num) will be affected according to the value of self.n_expo and length of the result
                           If self.n_expo>1, t in exposures, if you want a result in iterations it will be stored as num
                           If self.n_expo == 1 and len(res) > 20, we have a long result -> it is a number of iterations, every element of res will be stored with varying t values
                           If self.n_expo == 1 and we have len(res) < 21, it's a list of non - temporal data, every element of res will be stored with varying num values
        :param reinit: A boolean parameter that determines whether to reinitialize the lexicon at each simulation or not.
        :param lenMin: The minimum length of a word in the list of stimuli, defaults to 4 (optional)
        :param lenMax: The maximum length of a word in the list of stimuli, defaults to 8 (optional)
        :param n_stim_len: The number of stimuli of each length to generate for the simulation, defaults to 10 (optional)
        :param liste: A list of strings representing the stimuli to be used in the simulation. If not provided, a random list of stimuli will be generated based on the values of lenMin, lenMax, and n_stim_len
        :param word_action: A string representing the name of the function to be applied on a word before the simulation (ex change its frequency)
        :param word_action_value: word_action_value is an eventual function parameter (ex the frequency to be assigned). It is a dico used as kwargs.
        :param simulation_action: The simulation_action parameter is used to specify an action to be performed before the simulation, for each word and each exposure.
            prototype is f(t,**value)
            ex explicit learning : force learning for t=0 and force update for t>0
        :param simulation_action_value: The dico containing values to be passed as an argument to the simulation_action function when it is called (kwargs)
        :param shuffle: A boolean parameter that determines whether the list of stimuli should be shuffled before running the simulation.
        :param print_res: A boolean parameter that determines whether or not to print the results of the simulation.
        :param store_simu: A boolean parameter that determines whether to store the simulation object or not.
        """
        self.simu=simu
        self.res_fct_name = res_fct_name if isinstance(res_fct_name, list) else [res_fct_name]
        self.path=path
        self.basename=basename
        self.test = test
        self.n_expo = n_expo
        self.reinit = reinit
        self.lenMin, self.lenMax, self.n_stim_len = lenMin,lenMax,n_stim_len
        self.liste=liste if liste is not None else self.get_random_list(n_stim_len,lenMin,lenMax)
        self.word_action = word_action
        self.word_action_value=word_action_value
        self.simulation_action=simulation_action
        self.simulation_action_value=simulation_action_value
        self.shuffle=shuffle
        self.print_res=print_res
        self.store_simu=store_simu
        self.csv_name=None
        self.copy_model=None
        self.dico = self.already_tested_words = None
        self.succ = True
        self.last_expo_isolation=last_expo_isolation


    def update(self, **kwargs):
        """ used to set several attributes at once"""
        for k, v in kwargs.items():
            setattr(self, k, v)

    ########################
    ### Beginning of expe ##
    ########################

    def get_random_list(self, nb, lenMin, lenMax):
        """
        Get list of random stimuli from the lexicon
        """
        # words with '~' are words fragments, not real words, and cannot be used as stimuli
        lx = self.simu.model.ortho.lexical.df
        lx=lx[~lx.index.str.contains('~')].reset_index().groupby('word').first()
        if 'pron' in lx.columns:
            # no selection of homophones
            hp = lx.groupby('pron').count()['idx'].reset_index()
            hp = list(hp[hp.idx > 1].pron.values)
            lx=lx[~lx.pron.isin(hp)].copy()
        return list(lx[(lx.freq > 0) & (lx.len >= lenMin) & (lx.len <= lenMax) & (~lx.index.str.contains('_'))].groupby('len').sample( n=nb, random_state=24).index)

    def set_filename(self):
        """
        Sets the filenames for saving simulation data in pickle and csv formats.
        """
        #if not os.path.exists('pkl'):
        #    os.mkdir('pkl')
        if not os.path.exists('csv'):
            os.mkdir('csv')
        if not os.path.exists('pkl'):
            os.mkdir('pkl')
        o=self.simu.model.ortho.lexical.remove_stim
        p=self.simu.model.phono.lexical.remove_stim
        name=self.basename + '_PM_'+('X' if not (o or p) else 'O'*o+'P'*p)
        self.csv_name = 'csv/' + name + '.csv'
        logging.expe(f"{self.liste} \n {self.csv_name}")

    def load_existing_data(self):
        """
        Loads preliminary results if part of the simulation was already conducted.
        """
        try:
            df = pd.read_csv(self.csv_name)[['num','t','value','word','success','error_type']+list(self.test.keys())]
        except:
            df = self.initialize_data()
        df = df[df.word.isin(self.liste)]
        self.dico = df.to_dict('list')
        try:
            self.already_tested_words = list(set(df.word))
        except:
            self.already_tested_words = []
            pdb.set_trace()

    def initialize_data(self):
        """
        This function initializes the dictionary that will contain the results.
        """
        self.dico = dict(**{'num': [], 't': [], 'word': [], 'value': [], 'success': [], 'error_type': []}, **{key: [] for key in self.test})
        return pd.DataFrame.from_dict(self.dico)

    def begin_expe(self):
        """
        Sets up various parameters and variables before the simulation begins.
        """
        self.set_filename()
        self.load_existing_data()
        self.param_product = [dict(zip(self.test, x)) for x in itertools.product(*self.test.values())]
        self.simu.model.ortho.lexical.build_all_repr()
        self.simu.model.phono.lexical.build_all_repr()
        try:
            self.copy_model = copy.deepcopy(self.simu.model) if self.reinit else None
        except:
            pdb.set_trace()



    ##############################################################
    ### Result function (used by compare_param end RealSimulation)
    ##############################################################

    def res_fct(self):
        """
        This function concatenates the results of different function names and returns them.
        :return: a list of results obtained by calling the `one_res` method of the `simu` object for each name in the
        `res_fct_name` list. If there is only one result and it is a list with more than one element, it returns the list. If there is more than one
        result and they are not strings, it concatenates them.
        """
        res=[self.simu.one_res(i) for i in self.res_fct_name]
        if len(self.res_fct_name)>1 and not isinstance(res[0],str):
            try:
                return np.concatenate((res[0], res[1:]))
            except:
                return res
        return res


    ####################
    ### Data storing ###
    ####################


    def store_res(self):
        """
        Stores the results in a pkl file
        """
        try:
            df = pd.DataFrame.from_dict(self.dico)
        except:
            logging.error("Erreur dans la création du dataframe")
            print(self.dico)
            pdb.set_trace()
        if self.store_simu:
            cp = copy.deepcopy(self.simu);
            # removes heavy part of the object before storing it
            cp.ortho.all_repr={}
            cp.ortho.repr=[]
            if self.simu.model.phono.enabled:
                cp.phono.all_repr={}
            cp.phono.repr=[]
        pkl.dump([df, self.test, cp if self.store_simu else None], open(self.path + self.csv_name, 'wb'))
        df.to_csv(self.csv_name)
        return df

    def intermediate_store(self, iw):
        """
        Stores the results if needed (not at each exposure -> time saved)
        """
        step = 5
        if iw % step == step - 1:
            df = self.store_res()
            if iw == step - 1 and self.print_res:
                logging.expe(df)

    def end_expe(self):
        """
        Actions to perform at the end of the experiment : printing a dataframe and saving it to a CSV file if specified.
        :return: the dictionary `self.dico` which contains the raw results.
        """
        df = self.store_res()
        if self.print_res:
            print(df)
        print("end of expe")
        df.to_csv(self.csv_name)
        return self.dico

    ####################
    ### Big simulations ###
    ####################

    def compare_param(self):
        """ Generic simulation to compare different values of parameters :
            for example max_iter, Q, leak ...
            it sets automatically the value of the parameter, given the name of the parameter (ex "Q","max_iter")
            and its possible values (ex [1,2],[250,500]) and test all combinations of parameters ex [1,2]x[250,500] -> 4
            /!\ In these simulations, all words are tested independantly

            If you wqnt to run this kind of simulation with a new parameter, you have to :
                1/ choose/define a new name in the simu.one_res function to define how to get the result you're interested in.
                2/ define how you set the parameter in the set_attr function from class simu (if it's not automatic)
        """
        self.begin_expe()
        for iw, word in enumerate(self.liste):
            if not word in self.already_tested_words:
                logging.expe(f"{word} {iw}/{len(self.liste)}")
                for ip, indices in enumerate(self.param_product):
                   if self.reinit:
                       self.simu.model = copy.deepcopy(self.copy_model)
                   self.simu.model.ortho.stim = word
                   self.simu.reset_n()
                   for p, val in indices.items():
                        setattr(self.simu, p, val)
                   if self.word_action:
                       self.word_action(word, **self.word_action_value)
                   for t in range(self.n_expo):
                       if self.last_expo_isolation and t==self.n_expo-1:
                           ctxt=self.simu.model.semantic.context_sem
                           self.simu.model.semantic.context_sem=False
                       if self.simulation_action:
                           self.simulation_action(t, **self.simulation_action_value)
                       self.simu.run_simu_general()
                       res = self.res_fct()
                       succ = [self.simu.success(r) for r in self.res_fct_name]
                       if isinstance(succ[0],list) and len(succ[0])>1:
                           succ = np.concatenate((succ[0], succ[1:])) if len(succ)>1 else succ[0]
                       if self.print_res:
                           print(word,indices,[round(i,4) if isinstance(i,float) else i for i in res])
                       self.simu.increase_n()
                       for ir, r in enumerate(res): # 2D array with time as second dimension
                           if isinstance(r, np.ndarray) :
                               for iit, it in enumerate(r):
                                   app = dict(**{'word': word, 't': iit, 'num': ir, 'value': it, 'success': succ[ir],'error_type':self.simu.error_type}, **indices)
                                   for k, v in app.items():
                                       try:
                                           self.dico[k].append(v)
                                       except:
                                            pdb.set_trace()
                           else:
                               # /!\ t can be either a number of iterations or a number of exposures
                               # but in the simulation it's possible to vary both the number of exposures and store one result per iteration simulated
                               # If self.n_expo>1, time in exposures, if you want a result in iterations it will be stored as num
                               # If self.n_expo == 1 and len(res) > 20, we have a long result -> it is a number of iterations(time=ir, num=0)
                               # If self.n_expo == 1 and we have len(res) < 21, it's a list of non - temporal data(num=ir, time=t)
                               time = ir if (self.n_expo == 1 and len(res) > 20) else t
                               num = 0 if (self.n_expo == 1 and len(res) > 20) else ir
                               try:
                                   app = dict(**{'word': word, 't': time, 'num': num, 'value': r, 'success': succ[ir],'error_type':self.simu.error_type}, **indices)
                               except:
                                    pdb.set_trace()
                               for k, v in app.items():
                                   self.dico[k].append(v)
                               if self.last_expo_isolation and t == self.n_expo - 1:
                                   self.simu.model.semantic.context_sem = ctxt
                self.intermediate_store(iw)
                gc.collect()
        self.end_expe()

