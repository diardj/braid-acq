# BRAID utlities
import copy
import pdb
from time import time

import numpy as np
import braidpy.utilities as utl
import braidpy.lexicon as lex
from scipy.stats import entropy
import logging


class percept:
    """
    The percept class is an inner class of the modality class and represents the perceptual submodel, either in the orthographic or phonological modality.

    :param leak: float. Parameter for the decline of the percept distribution
    """
    def __init__(self,modality, leak, gamma_ratio, top_down):
        self.modality=modality
        self.leak = leak
        self.gamma_ratio=gamma_ratio
        self.top_down=top_down
        self.gamma_sem_init=0
        self.limited_TD=True # only the first 50 representation to limit calculation cost
        self.dist = {}

    #################################
    #### INFERENCES #################
    #################################

    #### Calcul Gamma ##############

    def sigm(self,val):
        """
        Calculates the top-down lexical retroaction strength

        :param val: input value to the sigmoid function (generally the value of the lexical decision distribution)
        :return: the top-down lexical retroaction strength
        """
        return 2e-6 + 1 * (self.gamma_ratio / np.power((1. + np.exp(-(97 * val) + 95)), .7))

    @utl.abstractmethod
    def gamma(self):
        """
        Updates the gamma coefficient for top-down lexical retroaction
        """
        pass

    def init_gamma_sem(self):
        sem = self.modality.model.semantic
        if sem.top_down:
            self.dist["gamma_sem"] = np.dot(self.modality.word.dist["word"], sem.dist["sem"])
            self.dist["sim_sem"] = 1
            self.gamma_sem_init = self.dist["gamma_sem"]

    def gamma_sem(self):
        """
        Calculates values for gamma_sem and gamma_sim based on the similarity between the semantic distribution and the word distribution.
        """
        sim = np.dot(self.modality.word.dist["word"], self.modality.model.semantic.dist['sem']) / self.gamma_sem_init
        gamma = 2 * self.modality.word.gamma_ratio / np.power((1. + np.exp(-3 * sim + 11)), .7)
        self.dist["gamma_sem"] = gamma
        self.dist["sim_sem"] = sim

    ##### Calcul probability distributions ########

    @utl.abstractmethod
    def build_percept(self):
        """
        Builds the percept distribution with bottom-up information
        """
        return

    def update_percept(self):
        """
        Updates the percept distribution with the top down retroaction
        """
        if self.modality.enabled and self.modality.model.semantic.top_down:
            if self.limited_TD:
                idx=self.modality.word.dist['word'].argsort()[::-1][:50]
                dist = utl.TD_dist(self.modality.word.dist["word"][idx], self.modality.lexical.repr[:self.modality.lexical.shift_begin][idx])
            else:
                dist = utl.TD_dist(self.modality.word.dist["word"], self.modality.lexical.repr[:self.modality.lexical.shift_begin])
            self.dist["percept"]*= (self.modality.word.dist["gamma"] * dist + (1 / self.modality.n) * (1 - self.modality.word.dist["gamma"]) * np.ones(self.dist["percept"].shape))
            self.dist["percept"]= utl.norm_percept(self.dist["percept"])



    def update_percept_sem(self):
        """
        Updates the percept distribution with the top down retroaction according to the semantic context
        """
        if self.modality.enabled and self.modality.model.semantic.top_down:
            dist = utl.TD_dist(self.modality.word.dist["word"], self.modality.lexical.repr[:self.modality.lexical.shift_begin])
            self.dist["percept"] *= (self.dist["gamma_sem"] * dist + (1 / self.modality.n) * (1 - self.dist["gamma_sem"]) * np.ones(self.dist["percept"].shape))
            self.dist["percept"] = utl.norm_percept(self.dist["percept"])

    #################################
    #### INFO #######################
    #################################

    def get_entropy(self,dist=None):
        """
        Calculates the entropy of the percept distribution

        :param dist: a distribution in the same shape as self.dist["percept"]. If None, returns the percept entropy.
        :return: The entropy of the distribution, one value per position.
        """
        dist = dist if dist is not None else self.dist["percept"]
        return [entropy(i) for i in dist]

    #################################
    #### DECISIONS ##################
    #################################

    @utl.abstractmethod
    def decision(self,dist=None):
        pass


    def evaluate_decision(self,dist=None):
        """
        Evaluates the percept decision taken by the model (function decision of this class).

        :param dist: a distribution in the same shape as self. If None, the model state is used instead.
        :return: a boolean value indicating whether the decision is correct.
        """
        dist = dist if dist else self.dist["percept"]
        return self.modality.stim == utl.str_transfo(self.decision(dist))

    def print_dist(self):
        """
        Prints information about the percept distribution (used at the end of a simulation)

        :param dist_name: The name of the distribution to be printed.
        :return: a string with information about the distribution.
        """
        if self.modality.enabled :
            return f' {self.decision()}, {[round(np.max(i),4) for i in self.dist["percept"]]}'
        return ''


class perceptOrtho(percept):
    def __init__(self, modality, top_down=True, gamma_ratio=1.45e-4,leak = 1.5e-5, **modality_args):
        super().__init__(modality=modality, top_down=top_down, gamma_ratio=gamma_ratio,leak = leak,  **modality_args)

    #################################
    #### INFERENCES #################
    #################################

    def gamma(self):
        self.dist["gamma"] = self.sigm(self.dist["ld"][0])

    def build_percept(self):
        """
        Builds the orthographic percept distribution
        """
        mem = (self.dist["percept"] + self.leak) / (1 + self.modality.n * self.leak)
        self.dist["percept"] = utl.norm_percept(mem * self.bottom_up_matrix)

    #################################
    #### DECISIONS ##################
    #################################

    def decision(self,dist=None):
        """
        Takes a decision on the most probable letter in each position (maximum of the probability distribution)
        """
        dist= dist if dist is not None else self.dist["percept"]
        return "".join([self.modality.chars[i] if i > -1 else '~' for i in [np.argmax(dist[i, :]) for i in range(np.shape(dist)[0])]])


    def print_dist(self):
        return "percept ortho : " + super().print_dist()


class perceptPhono(percept):
    def __init__(self, modality, top_down=True, gamma_ratio=1e-3,leak = 0.000015,  placement_auto=True, **modality_args):
        super().__init__(modality=modality,top_down=top_down,gamma_ratio=gamma_ratio,leak=leak, **modality_args)
        self.use_word=False
        self._pos = -1
        self.used_idx, self.used_idx_tmp, self.used_mask ={key:[] for key in range(30)},[],[]
        # better results with True, would like to be able to put it at False
        self.placement_auto=placement_auto

    #################################
    #### INFERENCES #################
    #################################

    def gamma(self):
        self.dist["gamma"] = self.sigm((1 if self.word_reading else self.dist["ld"][0]))

    def filter_att(self,dist=None, att=None):
        """
        Calculates the perceptual distribution filtered by attention.

        :param dist: the percept distribution. If None, takes the model current percept distribution.
        :param att: the attention profile, which is a list of floats between 0 and 1. If None, takes the model current attentional distribution.
        :return: The filtered percept distribution
        """
        att = att if att is not None else self.modality.attention.dist
        dist = dist if dist is not None else self.dist["percept"]
        return np.array([i * a + (1 - a ) / self.modality.n for (i, a) in zip(dist,att)])

    def select_phono_repr(self):
        """
        Selects the phonological representations that will be used during decoding based on the similarity between letter percept and orthographic representations.

        :return: the number of words selected and the phonological representations selected.
        """
        mask = utl.norm1D(self.modality.model.ortho.word.dist["word_sim_att"])
        nb_words = min(10, len(mask))
        # faster than argsort : selects 10 maximum values
        idx_sort = np.sort(np.argpartition(mask, len(mask) - nb_words)[-nb_words:])
        self.used_idx_tmp = [i for i in idx_sort if mask[i] > 0]  # for the case where there are less than 10 words in the lexicon
        self.used_idx[self.modality.model.ortho.pos] = list(dict.fromkeys(self.used_idx[self.modality.model.ortho.pos] + self.used_idx_tmp))
        self.used_mask = [mask[i] for i in self.used_idx_tmp]
        if logging.DEBUG >= logging.root.level:
            logging.debug(f"used words {self.modality.model.ortho.lexical.get_names(self.used_idx_tmp)}")
            logging.debug(f"sim value {utl.l_round(self.used_mask)}")
        logging.debug(f"sim value {[mask[i] for i in self.used_idx_tmp]}")
        logging.debug(f"psi : {self.decision()}")
        return len(self.used_idx_tmp), copy.copy(self.modality.lexical.repr[self.used_idx_tmp])


    def phono_alignment(self, nb_words, phono_repr):
        """
        Determines if the selected phonological representations need to be shifted left or right to improve the alignment, and performs the shift if necessary.
        To do that, the model calculates for each phonological representation the similarity between the phonological percept and the phonological representation,
        which can be either left untouched, shifted left or right. If similarity is higher for a shifted representation, it's done this way.

        :param nb_words: int. Number of phonological words selected.
        :param phono_repr: np.array. The selected phonological representations.
        :return: the updated phono_repr array after performing phonological alignment on it.
        """
        psi = self.dist["percept"]
        ref = self.modality.lexical.repr[self.modality.lexical.get_word_entry().idx] if self.placement_auto and len(self.modality.stim) > 0 else psi
        if sum(sum(ref)) == 0:
            ref = utl.create_repr(np.array([[self.modality.chars.index(i) for i in self.modality.stim + '#' * (self.modality.M - len(self.modality.stim))]]), self.modality.n, self.modality.lexical.eps)[0]
        unif = np.ones(self.modality.n) / self.modality.n
        # shifted phono representations
        repr_decal = np.zeros((3, nb_words, self.modality.M, self.modality.n))
        repr_decal[0] = np.concatenate((phono_repr[:, 1:], np.repeat(unif[np.newaxis, np.newaxis], nb_words, axis=0)), axis=1)
        repr_decal[1] = phono_repr
        repr_decal[2] = np.concatenate((np.repeat(unif[np.newaxis, np.newaxis], nb_words, axis=0), phono_repr[:, :-1]), axis=1)
        # alignment precisely on the precessed segment, not at other positions -> comparison filtered by attention
        logging.debug(f"attention profile phono : {utl.l_round(self.modality.attention.dist / self.modality.attention.Q)}")
        sim1 = np.einsum('jk,xijk->xij', ref, repr_decal)
        sim = np.array([[np.prod(self.filter_att(i, att=utl.norm1D(self.modality.attention.dist))) for i in w] for w in sim1])
        for i in range(nb_words):
            if i in range(len(self.used_idx_tmp)):
                S = sim[:, i]
                amax = np.argmax(S)
                if amax != 1 and max(S) > 1.1 * S[1]:
                    if logging.DEBUG >= logging.root.level:
                        logging.debug(f"dec {'gauche' if amax == 0 else 'droit'}, {self.decision('percept', phono_repr[i])}, {S}")
                    phono_repr[i] = repr_decal[amax, i]
                else:
                    if logging.DEBUG >= logging.root.level:
                        logging.debug(f"pas de décalage, {self.decision('percept', phono_repr[i])},  {S}")
        return phono_repr


    def calculate_psi(self, phono_repr):
        """
        Updates the psi distribution according to the phonological representations previously selected and possibly shifted.

        :param phono_repr: the phonological representations selected.
        """
        ph_tmp = utl.norm_percept(np.einsum('i,ijk->jk', utl.norm1D(self.used_mask), phono_repr))
        logging.debug(f"decision : {self.decision(dist=ph_tmp)}")

        ## Filtering of lexical information by phonological attention
        filt_ph = utl.norm_percept(np.array([i * a + (1 - a) / self.modality.n for (i, a) in zip(ph_tmp, self.modality.attention.dist)]))

        # Phonological STM and output distribution
        mem_ph = (self.dist["percept"] + self.leak) / (1 + self.modality.n * self.leak)
        self.dist["percept"] = utl.norm_percept(mem_ph * filt_ph)


    def build_decoding(self):
        """
        Decoding step : the model has previously selected the most similar words to the letter percept where the attention stands, it's stored in the distribution "word_sim_att".
        It now uses the phonological representations of those words, filtered by phonological attention, to update the psi distribution.
        These representations may possibly be shifted left or right, to ensure a better alignment between the actual phonological percept and
        the filtered phonological representations.
        """
        if self.modality.enabled:
            nb_words, phono_repr = self.select_phono_repr()
            if self.modality.attention.mean > 0:
                phono_repr = self.phono_alignment(nb_words, phono_repr)
            self.calculate_psi(phono_repr)

    #################################
    #### INFO #######################
    #################################

    def psi_score(self):
        """
        Computes the cosine similarity between the representation of the stimulus and the representation of the phonological percept distribution.

        :return: The mean of the cosine similarity between the representation of the stimulus and the representation of the percept.
        """
        if len(self.modality.stim)>0:
            n=len(utl.str_transfo(self.modality.stim))
            p=self.dist["percept"][:n]
            # on compare pas avec la représentation stockée mais avec une représentation adulte
            # comme ça la référence est toujours la même
            wds_idx = self.modality.lexical.get_repr_indices([self.modality.stim], self.modality.model.max_len[self.modality.N])
            repr=utl.create_repr(wds_idx, self.modality.n, self.modality.lexical.eps)[0]
            scal = np.einsum('ij,ij->i', p, repr)
            norm = np.sqrt(np.einsum('ij,ij->i', repr, repr)) * np.sqrt(np.einsum('ij,ij->i', p, p))
            return np.mean(scal/norm) if sum(norm)>0 else 0
        return-1

    def get_used_words(self):
        """
        A dictionary of the words used for decoding when attention landed on a specific position.

        :return: A dictionary of the used words in the format { position : [used_words] }
        """
        return {key:self.modality.model.ortho.lexical.get_names(list(dict.fromkeys(self.used_idx[key]+self.used_idx_tmp))) for key,value in self.used_idx.items() if len(value)>0}


    #################################
    #### DECISIONS ##################
    #################################

    def decision(self,dist=None):
        """
        Returns a string representing the pronunciation based on the maximum values of the phonological percept distribution.

        :param dist: 2D numpy array. The distribution of probabilities for each phoneme in a given word.
        :return: a string that represents the pronunciation of the word.
        """
        if isinstance(dist,str):
            raise TypeError("distribution should be an array of float values")
        dist= dist if dist is not None else self.dist["percept"]
        maxi = [max(d) for d in dist][::-1]
        last_idx = len(dist) - 1 - (next(i for i, val in enumerate(maxi) if val > 0.05) if max(maxi) > 0.05 else 0)
        pron_idx = [np.argmax(dist[i, :]) if max(dist[i, :]) > 0.05 and list(dist[i, :]).count(dist[i, 0]) != len(dist[i, :]) and (
                i <= last_idx or not (self.modality.enabled and self.modality.lexical.repr_unif)) else -1 for i in range(np.shape(dist)[0])]
        return "".join([self.modality.chars[i] if i > -1 else '~' for i in pron_idx])

    def print_dist(self):
        return "percept phono : " + super().print_dist()

