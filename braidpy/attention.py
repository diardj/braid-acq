import logging
import math
import pdb

import braidpy.utilities as utl
import braidpy.lexicon as lex
import numpy as np


class attention:
    """
    The attention class is an inner class of the modality class and represents the attentional submodel, either in the orthographic or phonological modality.

    """
    def __init__(self,modality,Q,mean,sd,sdM=5, segment_reading=False):
        """
        Attention class constructor.

         :param mean : float, position of the attentional focus
         :param sd : float, attentional dispersion
         :param sdM : float, max value of the attentional dispersion
         :param segment_reading: boolean. If set to True, the attention distribution is not Gaussian but uniform on 1 or several letters corresponding to one phoneme.
        """
        self.modality=modality
        self.Q=Q
        self.mean=mean
        self.sd = sd
        self.sdM=sdM
        self.dist=None
        self.segment_reading=segment_reading

    @property
    def mean(self):
        return self._mean

    @mean.setter
    def mean(self, value):
        """
        Sets attention position, starts at 0. Position should be set at -1 at the end of a simulation

        :param value: int, the position to be set.
        """
        if value < -1 or ('lexical' in self.__dict__ and self.modality.N is not None and value >= self.modality.N):
            logging.warning(f"bad mean position is trying to be set : {value}")
        self._mean = value

    @property
    def sd(self):
        return self._sd

    @sd.setter
    def sd(self, value):
        if value < 0:
            logging.warning("You're trying to set a negative value of sdA")
        sdM=self.__getattribute__("sdM") if hasattr(self,'sdM') and self.sdM is not None else 10000
        self._sd = min(value,sdM)

    @property
    def sdM(self):
        return self._sdM

    @sdM.setter
    def sdM(self, value):
        if value < 0:
            logging.warning("You're trying to set a negative value of sdM")
        self._sdM = value
        self.__setattr__('sd', self.sd)

    @utl.abstractmethod
    def build_attention_distribution(self):
        """
        Builds the attention distribution according to the attentional position mean, the standard deviation sd,
        the attentional Quantity Q and the length of the stimulus N.
        """
        pass

class attentionOrtho(attention):
    def __init__(self, modality, Q = 1, mean=-1, sd=1.75, sdM=5,**modality_args):
        super().__init__(modality=modality,Q=Q, mean=mean, sd=sd,  **modality_args)
        self.sdM = sdM


    def init_pos_auto(self):
        """
        Automatically sets the visual attention position (and also the gaze position) at the beginning of the simulation
        """
        if self.Q > 0.7 and self.sd > 1 :
            pos = [0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4][self.modality.N - 1]
        else:
            pos=0
        self.modality.pos = pos

    def build_attention_distribution(self):
        if self.mean >= 0:
            if self.segment_reading:
                gs=self.modality.model.gs
                len_grapheme=gs.count(gs[self.mean])
                new_mean=gs.index(gs[self.mean])
                tmp = np.array([1/len_grapheme if 0<=i-new_mean<len_grapheme else 0 for i in range(self.modality.M)])
            else :
                tmp = utl.gaussian(self.mean, self.sd, self.modality.M)
            tmp = self.Q * tmp
            tmp[tmp > 1] = 1
            self.dist = tmp


class attentionPhono(attention):
    def __init__(self, modality, Q = 0.005, mean=-1, sd=2, att_phono_auto=True, segment_reading=False, **modality_args):
        """
        :param att_phono_auto: boolean. if True, automatically sets the phonological attention according to the graphemic segmentation
        """
        super().__init__(modality=modality,Q = Q, mean=mean, sd=sd, segment_reading=segment_reading,**modality_args)
        self.att_phono_auto = att_phono_auto
        self.coupling_a = self.coupling_b = None


    def set_regression(self):
        """
        Performs linear regression to find the relationship between orthographic and phonological length.
        Use for attention coupling (matching orthographic positions to phonological positions)
        """
        from sklearn import linear_model
        x = self.modality.model.ortho.lexical.df.len.values.reshape(-1, 1)
        y = self.modality.lexical.df.len.values.reshape(-1, 1)
        reg = linear_model.LinearRegression()
        res = reg.fit(x, y)
        self.coupling_a, self.coupling_b = res.coef_[0][0], res.intercept_[0]

    def len2phlen(self,x,rnd=1):
        """
        Calculates the predicted phonological length as a function of the orthographic length according to the linear regression.

        :param x: the orthographic length
        :return: the predicted phonological length
        """
        return round(float(self.coupling_b + (self.coupling_a * x))/ rnd) * rnd

    def calculate_attention_parameters(self):
        """
        Chooses the next phonological position according to statistical properties of the word or according to the graphemic segmentation (if enabled)
        """
        if self.att_phono_auto and self.modality.model.ortho.stim in self.modality.model.df_graphemic_segmentation.index:
            self.modality.pos = int(self.modality.model.gs[self.modality.model.ortho.pos])
        elif self.att_phono_auto:
            logging.simu("ATTENTION : segmentation graphémique absente!")
            pdb.set_trace()
        else:
            self.modality.pos =round(self.len2phlen(self.modality.model.ortho.pos)) if self.modality.model.ortho.pos > 0  and self.coupling_a is not None else 0
        self.sd = self.len2phlen(self.modality.model.ortho.attention.sd,rnd=0.25)

    def build_attention_distribution(self):
        """
        Calculates the attention parameters and builds the attention distribution according to it.
        """
        if self.mean >= 0:
            tmp = np.array([1 if i == self.mean else 0 for i in range(self.modality.M)])  if self.segment_reading \
                    else utl.gaussian(self.mean, self.sd, self.modality.M)
            tmp = self.Q * tmp
            tmp[tmp > 1] = 1
            self.dist = tmp

