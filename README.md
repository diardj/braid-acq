
# BraidPy
Python implementation of BRAID model.
Reading, Visual word recognition and lexical Decision

## Installation of the python package (optional)

In the root directory, run the following command :

```python
python setup.py install
```

If you want to modify the source code, use instead :

```python
python setup.py develop
```
## Dependencies (only if you don't use Conda)

Here is the needed packages to run the model and to plot the results:
install using Conda (either through anaconda GUI or command-line)

| Required 	| For fast  computation |        Data viz(optional) | 
| :------------ | -------------------   | ------------------------  |
| Numpy / Pandas|   Numba  	   	| mpl_toolkits		    |
| Matplotlib    |   Dask    		| bokeh			    |
| Seaborn       |   Joblib      	| 			    |

Useful packages but not required: 
unidecode,  Sklearn, StatsModels

## Conda Environment  
  
If you want to directly use the conda environment with the good Python/libraries version, you can use the environment file braid_env_30092022.yml
  
To create the braid environment, run the following command (after installing conda) in the braid root directory : 
```python
conda env create -f env/braid_env_10092023.yml   
```
It will create a new environment named braid_env.
  
To go in this environment, run the following command : 
```python
conda activate braidacq_env
```

There is still some libraries to install :
```python
pip3 install Levenshtein sklearn 
```

If you want to create your own yml file, run the following command in the env/ directory: 
```python
./export_env.sh filename.yml
```

## Current Features
The current implementation performs simulations of:
- Letter perception 
- Visual Word recognition
- Lexical Decision
- Word reading (decoding)
- Novel Word Acquisition

## Code architecture

There are several main classes :
- the "simu" class, which makes specific simulations (i.e. tasks) using the BRAID model and stores the result. It contains one inner class, the "braid" class.
- the "braid" class, which is the model class. It contains 2 inner classes, the "ortho" and "phono" classes, that are derived from the "modality" class.
- the "modality" class, which implements calculations specific to one modality. It contains several inner classes, that correspond to the BRAID-Acq submodels : "sensor", "percept", "word", "lexical", "attention".
- the "semantic" class, which implements calculations related to the semantic context.

The architecture of the code is illustrated on the following UML diagram:


![Architecture of the BRAID-Acq code](UML.png)

For more information, see the full documentation of the code.

## Usage

To learn how to conduct simulations with the model BRAID-Acq, look at the notebooks in the directory notebooks/ :

* first_word.ipynb : learn to easily write Python code to simulate the reading of one word.
* command_line.ipynb : learn to simulate the reading of one word using the command-line.
* first_simulations.ipynb : learn how to simulate an entire experiment on several words, through the example of length and lexicality effects.


## Class documentation

The class documentation is accessible at: build/html/modules.html.

If you modified the code and want to re-generate the doc, use the following command in the root directory:
```bash
make html
```

If you want to generate the doc from the start, you can look at the sphinx and auto-doc documentation documentation. You can also use these steps:

1. If they exist, delete source/ and build/ directories.
2. Initialize the documentation by the following command in the root directory:

```bash
sphinx-quickstart
```
3. Add the autodoc extension in the file conf.py in the source/ directory:

```bash
extensions = ['sphinx.ext.autodoc']
```
4. Automatically find modules in code

```bash
sphinx-apidoc -o source/ ../braidpy
```


5. Type the following command in the root directory:
```bash
make html
```


## Contact

email  
Alexandra Steinhilber : alexandra.st@free.fr / alexandra.steinhilber@univ-grenoble-alpes.fr  
Julien Diard : julien.diard@univ-grenoble-alpes.fr
